# Introduction
This is the home page of System Analysis Template brought to you by DANA Indonesia.

Special Thanks to:
1. [Ariyandi Widarto](https://www.linkedin.com/in/ariyandi-widarto-b7456690/) - Engineering Architect of DANA Indonesia
2. [Wisnu Wardoyo](https://www.linkedin.com/in/wisnu-wardoyo-48a418ab/) - Engineering Architect of DANA Indonesia

Note : both of them are not open for any recruitments. Thanks

## SA Template
[Click Here](https://gitlab.com/wisnuwardoyo/sa-garuda-hack/-/blob/master/template/sa_template.md) to view SA Template

## Diagram
[Click Here](https://gitlab.com/wisnuwardoyo/sa-garuda-hack/-/tree/master/learning) to view diagram
