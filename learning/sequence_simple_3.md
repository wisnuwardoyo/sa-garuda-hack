```plantuml

actor user
entity web

participant "Gateway" as gate
participant "On Boarding\nSystem" as ob
participant "Security System" as sec

activate user

autonumber 1


== Open Registration Page==
user -> web+: open landing page\nclick register
web -> gate+: HTTP call
gate -> ob+: open register page
return register page\nand data
return register page
return render page

== Submit Registration Data==

user -> web+: submit data
web -> gate+: HTTP call
gate -> ob+: submit register data

alt if OTP = null | securityID = null
  autonumber 10.1.1
  ob -> sec+: send OTP
  return send result
else
  autonumber 10.2.1
  ob -> sec+: verify OTP
  return verify status

  autonumber 11
  ob -> ob: save data to DB
end


return register status
return register status
return render info

deactivate user

```
