```plantuml

'hide circle

Title Campaign Management Entity Diagram

entity "ob_user" as user {
  * id : bigint <<generated>>
  ---
  * user_id : varchar(64)
  * phone_number : varchar(20)
  * password : varchar(512)
  * nickname : varchar(128)
  * status : varchar(16)
    address : varchar(512)
    dob: date(6)
  * create_time : datetime DEFAULT TIMESTAMP
  * modified_time : datetime DEFAULT TIMESTAMP
  ---
  <<PK>>
    + id

  <<unique>>
  + uk_phone_number(phone_number)
  + uk_user_id(user_id)

}

entity "trx_transaction" as transaction {
  * id : bigint <<generated>>
  ---
  * transaction_id : varchar(64)
  * user_id : varchar(64)
  * amount : bigint
  * status : varchar(16)
  * create_time : datetime DEFAULT TIMESTAMP
  * modified_time : datetime DEFAULT TIMESTAMP
  ---
  <<PK>>
    + id

  <<FK>>
  + user_id

  <<unique>>
  + uk_transaction_id(transaction_id)

  <<index>>
  + idx_user_time(user_id, create_time DESC) NORMAL
  + idx_user_status_time(user_id, status, create_time DESC) NORMAL
}

'relation
user ||-right-|{ transaction : 1 to many

```
