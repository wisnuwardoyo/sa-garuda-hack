```plantuml

' State Definition
Active : Default state of user after register
Close : User account closed\nand cannot be revived
Freeze : User account frozen and cannot login\nBut user can be activated again


' State Relation
[*] --> Active
Active --> Close
Active --> Freeze
Freeze --> Active
Freeze --> Close
Close --> [*]

```
