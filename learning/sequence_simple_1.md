```plantuml

actor user
entity web

participant "Gateway" as gate
participant "On Boarding\nSystem" as ob
participant "Security System" as sec


user -> web: open landing page\nclick register
web -> gate: HTTP call
gate -> ob: open register page
ob --> gate: register page\nand data
gate --> web: register page
web --> user: render page

user -> web: submit data
web -> gate: HTTP call
gate -> ob: submit register data

alt if OTP = null | securityID = null
  ob -> sec: send OTP
  sec --> ob: send result
else
  ob -> sec: verify OTP
  sec --> ob: verify status

  ob -> ob: save data to DB
end


ob --> gate: register status
gate --> web: register status
web --> user: render info



```
